module API
  module V1
    module ErrorFormatter
      def self.call message, backtrace, options, env
        { :response_type => 'error 1', :response => message }.to_json
      end
    end
  end
end