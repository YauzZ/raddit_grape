# lib/api/v1/authors.rb
module API
  module V1
    class Authors < Grape::API
      version 'v1'
      format :json

      resource :authors do
        desc "Return list of authors"
        get do
          user = User.find_by_email(params[:email])
          if user && user.valid_password?(params[:password])
             { :response_type => 'sucess', :access_token => user.authentication_token }.to_json
          else
            error!("401 Unauthorized", 401)
          end
        end  

        post do
          user = User.find_by_email(params[:email])
          if user && user.valid_password?(params[:password])
             { :response_type => 'sucess', :access_token => user.authentication_token }.to_json
          else
            error!("401 Unauthorized", 401)
          end
        end

        def authentication
          
        end
      end
    end
  end
end