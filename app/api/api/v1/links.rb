
module API
  module V1
    class Links < Grape::API
      version 'v1'
      format :json

      resource :links do
        desc "Return list of recent posts"
        get do
          	Link.all
        end

        get ':id', requirements: { id: /[0-9]*/ } do
          Link.find(params[:id])
        end

        post do
        	#@link = current_user.links.build(params)

          params do
            requires :title, type: String
            requires :url, type: String
          end

          if params[:access_token] && @user = User.find_by_authentication_token(params[:access_token])
              user = User.find_by_authentication_token(params[:access_token])
              #link = Link.new
              link = user.links.new()
              link.title = params[:title]
              link.url = params[:url]

              if link.save
                link
              else
                error!("402 UnSave", 402)
              end
          else
            error!("401 Unauthorized", 401)
          end
        end
      end
    end
  end
end