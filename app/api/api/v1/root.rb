# lib/api/v1/root.rb
module API
  module V1
    class Root < Grape::API

    # before do
    #   error!("401 Unauthorized", 401) unless authenticated
    #  end

    #   helpers do
    #     def authenticated
    #       user = User.find_by_email(params[:email])
    #       user && user.valid_password?(params[:password])
    #     end
    #   end

    format :json
    error_formatter :json, API::ErrorFormatter

    rescue_from :all, :backtrace => true
    # error_formatter :json, API::ErrorFormatter


    helpers do
      def warden
        env['warden']
      end

      def authenticated
        # return true if warden.authenticated?
        params[:access_token] && @user = User.find_by_authentication_token(params[:access_token])
      end

      def current_user
        warden.user || @user
      end
    end

      mount API::V1::Posts
      mount API::V1::Authors
      mount API::V1::Links
    end
  end
end