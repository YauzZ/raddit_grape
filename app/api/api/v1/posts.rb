# lib/api/v1/posts.rb
module API
  module V1
    class Posts < Grape::API
      version 'v1'
      format :json

      before do
      error!("401 Unauthorized", 401) unless authenticated
      end

      resource :posts do
        desc "Return list of recent posts"
        get do
          Link.all
        end

        post do
          Link.last
        end
      end
    end
  end
end